app.controller("MainController", function ($scope) {
    $scope.items = [
        {
            id: 0,
            name: 'Banan',
            price: 3,
            carbs: 50,
            vitamins: ['C', 'B', 'A', 'E'],
            image: 'http://louloublog.pl/wp-content/uploads/2015/02/banan-1.jpg'
        },
        {
            id: 1,
            name: 'Marchew',
            price: 1,
            carbs: 5,
            vitamins: ['A'],
            image: 'http://www.uzuma.com/img/cms/Uzuma/Ingredients/Carrot/EN/Carrot1.jpg'
        },
        {
            id: 2,
            name: 'Cebula',
            price: 5,
            carbs: 10,
            vitamins: ['C'],
            image: 'http://www.rynek-rolny.pl/images/articles/560/c7bf9ba3d6a19e55b3257a34f584ce15-rijnsburger.jpg'
        },
        {
            id: 3,
            name: 'Pomidor',
            price: 2,
            carbs: 2,
            vitamins: ['A'],
            image: 'http://www.rynek-rolny.pl/images/articles/560/6f2cf77c0fd6f4e684f495082473011c-shutterstock-196948529-2.jpg'
        },
        {
            id: 4,
            name: 'Awokado',
            price: 15,
            carbs: 100,
            vitamins: ['B'],
            image: 'http://ambasadapacyfiku.pl/wp-content/uploads/2015/08/Awokado_resize.jpg'
        },
        {
            id: 5,
            name: 'Szparagi',
            price: 100,
            carbs: 40,
            vitamins: ['K', 'E'],
            image: 'http://budujesz.info//pliki/image/artykuly/warzywa/duze/szparagi2414.jpg'
        },
        {
            id: 6,
            name: 'Kalafior',
            price: 10,
            carbs: 35,
            vitamins: ['B', 'K'],
            image: 'http://www.naturhouse-polska.pl/wgrane_pliki/big_kalafior.jpg'
        }
    ];

    $scope.currentOrder = 'id';
    $scope.orderSelect = ['name', 'price', 'carbs'];

    $scope.newItemName = '';
    $scope.newItemPrice = '';
    $scope.newItemCarbs = '';
    $scope.newItemVitamins = '';
    $scope.newItemImage = '';

    $scope.addNew = function () {
        if ($scope.newItemName) {
            var index = $scope.items.length;
            var newItem = {
                id: index,
                name: $scope.newItemName,
                price: $scope.newItemPrice,
                carbs: $scope.newItemCarbs,
                vitamins: $scope.newItemVitamins.split(','),
                image: $scope.newItemImage
            };
            $scope.items.push(newItem);
        }
    };

    var getIndex = function (id) {
        for (var i = 0; i < $scope.items.length; i++) {
            if ($scope.items[i].id === id) {
                return i;
            }
        }
    };

    $scope.remove = function (id) {
        $scope.items.splice(getIndex(id), 1);
    };

    $scope.edit = function (id) {
        $scope.currentEditItem = $scope.items[getIndex(id)];
    };

    $scope.endEditing = function (id) {
        $scope.items[getIndex(id)] = $scope.currentEditItem;
        $scope.currentEditItem = null;
    };

    $scope.filterByPrice = function (item) {
        return (!$scope.minPrice || parseInt(item.price) >= parseInt($scope.minPrice))
            && (!$scope.maxPrice || parseInt(item.price) <= parseInt($scope.maxPrice))
    };
})
    .directive('item', function () {
        return {
            templateUrl: 'item.html'
        };
    });
