var express = require('express');
var app = express();

app.get('/products', function (req, res) {
    res.redirect('/index.html');
});
app.get('/', function (req, res) {
    res.redirect('/products');
});

app.use(express.static(__dirname + '/public'));

app.listen(3000, function () {
    console.log('Live at port 3000');
});
